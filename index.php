<?php get_header() ?>
<section id="home">
    <div class="banner-container"> 
        <?php
        $image = get_bloginfo('template_url') . "/images/banner-bg.jpg";
        if ($file = do_shortcode("[siteoptions key='imgSlider']")) {
            $image = $file;
        }
        ?>
        <img src="<?php echo $image ?>" alt="banner" />
        <div class="container banner-content">
            <div class="hero-text animated fadeInDownBig"> <a href="#" class="scroll-top logo animated bounceInLeft"><b>Vibrant</b></a>
                <h1 class="responsive-headline" style="font-size: 40px;">
                    <?php echo do_shortcode("[siteoptions key='textSlider']") ?>
                    <!--                    Presenting you beautiful <br/>
                                        Bootstrap Responsive website-->
                </h1>
                <a href="#basics" class="arrow-link"> <i class="fa fa-chevron-down fa-2x"></i> </a> 
<!--                <p>Awesome theme for your Business or Corporate site to showcase <br/>
                  your product and service.</p> -->
            </div>

            <!-- <a class="hero-button learn-more smoothscroll text-center" href="#features">Learn More</a>--> 
            <!-- <div class="hero-img"> <img src="images/homepage-1204-background-lapto.png" alt="" class="text-center animated fadeInUpBig"/></div>--> 
        </div>
    </div>
</section>
<section id="features" class="page-section colord features">
    <div class="container">
        <div class="heading text-center"> 
            <!-- Heading -->
            <h2><?php echo do_shortcode("[siteoptions key='features' title='true']") ?></h2>
            <p><?php echo do_shortcode("[subtitle key='features']") ?></p>
             <!--<p>At lorem Ipsum available, but the majority have suffered alteration in some form by injected humour.</p>-->
        </div>
        <div class="row">
            <?php echo do_shortcode("[siteoptions key='features' article=1]") ?>
            <?php /*
              <!-- FEATURES LEFT -->
              <div class="col-md-4 col-sm-4 features-left wow">
              <!-- FEATURE -->
              <div class="feature">
              <!-- ICON -->
              <div class="icon-container">
              <div class="icon"> <i class="icon_map_alt"></i> </div>
              </div>

              <!-- FEATURE HEADING AND DESCRIPTION -->
              <div class="fetaure-details">
              <h4 class="main-color">Responsive Design</h4>
              <p> Lorem ipsum dolor sit amet, ea eum labitur scsstie percioleat fabulas labore et dolor. </p>
              </div>
              </div>
              <!-- /END SINGLE FEATURE -->

              <!-- FEATURE -->
              <div class="feature">

              <!-- ICON -->
              <div class="icon-container">
              <div class="icon"> <i class="icon_gift_alt"></i> </div>
              </div>

              <!-- FEATURE HEADING AND DESCRIPTION -->
              <div class="fetaure-details">
              <h4 class="main-color">Bootstrap 3.2.2</h4>
              <p> Lorem ipsum dolor sit amet, ea eum labitur scsstie percioleat fabulas labore et dolor.</p>
              </div>
              </div>
              <!-- /END SINGLE FEATURE -->

              <!-- FEATURE -->
              <div class="feature">

              <!-- ICON -->
              <div class="icon-container">
              <div class="icon"> <i class="icon_tablet"></i> </div>
              </div>

              <!-- FEATURE HEADING AND DESCRIPTION -->
              <div class="fetaure-details">
              <h4 class="main-color">Cross-Browser Support</h4>
              <p> Lorem ipsum dolor sit amet, ea eum labitur scsstie percioleat fabulas labore et dolor. </p>
              </div>
              </div>
              <!-- /END SINGLE FEATURE -->

              </div>
              <!-- /END FEATURES LEFT -->

              <!-- PHONE IMAGE -->
              <div class="col-md-4 col-sm-4">
              <div class="phone-image wow"> <img src="<?php bloginfo('template_url') ?>/images/single-iphone.png" alt=""> </div>
              </div>

              <!-- FEATURES RIGHT -->
              <div class="col-md-4 col-sm-4 features-right wow" >

              <!-- FEATURE -->
              <div class="feature">

              <!-- ICON -->
              <div class="icon-container">
              <div class="icon"> <i class="icon_genius"></i> </div>
              </div>

              <!-- FEATURE HEADING AND DESCRIPTION -->
              <div class="fetaure-details">
              <h4 class="main-color">Awesome Fonts</h4>
              <p> Lorem ipsum dolor sit amet, ea eum labitur scsstie percioleat fabulas labore et dolor. </p>
              </div>
              </div>
              <!-- /END SINGLE FEATURE -->

              <!-- FEATURE -->
              <div class="feature">

              <!-- ICON -->
              <div class="icon-container">
              <div class="icon"> <i class="icon_lightbulb_alt"></i> </div>
              </div>

              <!-- FEATURE HEADING AND DESCRIPTION -->
              <div class="fetaure-details">
              <h4 class="main-color">Creative Design</h4>
              <p> Lorem ipsum dolor sit amet, ea eum labitur scsstie percioleat fabulas labore et dolor. </p>
              </div>
              </div>

              <!-- /END SINGLE FEATURE -->

              <!-- FEATURE -->
              <div class="feature">

              <!-- ICON -->
              <div class="icon-container">
              <div class="icon"> <i class="icon_ribbon_alt"></i> </div>
              </div>

              <!-- FEATURE HEADING AND DESCRIPTION -->
              <div class="fetaure-details">
              <h4 class="main-color">More Features</h4>
              <p> Lorem ipsum dolor sit amet, ea eum labitur scsstie percioleat fabulas labore et dolor. </p>
              </div>
              </div>
              <!-- /END SINGLE FEATURE -->

              </div>
              <!-- /END FEATURES RIGHT -->
             */ ?>
        </div>
    </div>
    <!--/.container--> 
</section>
<section id="aboutUs">
    <div class="container">
        <div class="heading text-center"> 
            <!-- Heading -->
            <h2><?php echo do_shortcode("[siteoptions key='about' title='true']") ?></h2>
            <p><?php echo do_shortcode("[subtitle key='about']") ?></p>
            <!--            <h2>About Us</h2>
                        <p>At lorem Ipsum available, but the majority have suffered alteration in some form by injected humour.</p>-->
        </div>
        <div class="row feature design">
            <div class="six columns right">
                <?php echo do_shortcode("[siteoptions key='about' article=1]") ?>
                <?php /* <p><strong>Lorem ipsum dolor sit amet, ea eum labitur scsstie percipitoleat fabulas complectitur deterruisset at pro. Odio quaeque reformidans est eu, expetendis intellegebat has ut, viderer invenire ut his. Has molestie percipit an. Falli volumus efficiantur sed id, ad vel noster propriae. Ius ut etiam vivendo, graeci iudicabit constituto at mea. No soleat fabulas prodesset vel, ut quo solum dicunt.
                  Nec et jority have suffered alteration. </strong></p>
                  <p>Odio quaeque reformidans est eu, expetendis intellegebat has ut, viderer invenire ut his. Has molestie percipit an. Falli volumus efficiantur sed id, ad vel noster propriae. Ius ut etiam vivendo, graeci iudicabit constituto at mea. No soleat fabulas prodesset vel, ut quo solum dicunt.
                  Nec et amet vidisse mentitumsstie percipitoleat fabulas. </p> */ ?>
            </div>
            <div class="six columns feature-media left"> 
                <?php echo do_shortcode("[featured key='about']") ?>
                <?php /* <img src="<?php bloginfo('template_url') ?>/images/feature-img-1.png" alt=""> */ ?>
            </div>
        </div>
    </div>
</section>
<section id="services">
    <div class="container services">
        <div class="heading text-center"> 
            <!-- Heading -->

            <h2><?php echo do_shortcode("[siteoptions key='service' title='true']") ?></h2>
            <p><?php echo do_shortcode("[subtitle key='service']") ?></p>
            <?php /* <h2>Services</h2>
              <p>At lorem Ipsum available, but the majority have suffered alteration in some form by injected humour.</p> */ ?>
        </div>
        <div class="serviceBox">
            <?php
            echo do_shortcode("[siteoptions key='service' article=1]");
            /* <div class="row">
              <div class="col-md-4 col-sm-4">
              <h3><i class="fa fa-desktop color"></i>&nbsp; User Experiance</h3>
              <!-- Paragraph -->
              <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
              </div>
              <div class="col-md-4 col-sm-4">
              <!-- Heading -->
              <h3><i class="fa fa-cloud color"></i>&nbsp;Story Board</h3>
              <!-- Paragraph -->
              <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
              </div>
              <div class="col-md-4 col-sm-4">
              <!-- Heading -->
              <h3><i class="fa fa-home color"></i>&nbsp;Wireframes</h3>
              <!-- Paragraph -->
              <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
              </div>
              </div>
              <div class="row">
              <div class="col-md-4 col-sm-4">
              <h3><i class="fa fa-desktop color"></i>&nbsp;Information</h3>
              <!-- Paragraph -->
              <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
              </div>
              <div class="col-md-4 col-sm-4">
              <!-- Heading -->
              <h3><i class="fa fa-cloud color"></i>&nbsp;Graphic Design</h3>
              <!-- Paragraph -->
              <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
              </div>
              <div class="col-md-4 col-sm-4">
              <!-- Heading -->
              <h3><i class="fa fa-home color"></i>&nbsp;User Interface</h3>
              <!-- Paragraph -->
              <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
              </div>
              </div> */
            ?>
        </div>
    </div>
</section>
<section id="work" class="page-section page">
    <div class="container">
        <div class="heading text-center">
            <h2><?php echo do_shortcode("[siteoptions key='works' title='true']") ?></h2>
            <p><?php echo do_shortcode("[subtitle key='works']") ?></p>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php
                echo do_shortcode(do_shortcode("[siteoptions key='works' article=1]"));
                /* <div id="portfolio">
                  <ul class="filters list-inline text-center">
                  <li> <a class="active" data-filter="*" href="#">All</a> </li>
                  <li> <a data-filter=".photography" href="#">Photography</a> </li>
                  <li> <a data-filter=".branding" href="#">Branding</a> </li>
                  <li> <a data-filter=".web" href="#">Web</a> </li>
                  </ul>
                  <ul class="items list-unstyled clearfix animated fadeInRight showing" data-animation="fadeInRight" style="position: relative; height: 438px;">
                  <li class="item branding" style="position: absolute; left: 0px; top: 0px;"> <a href="<?php bloginfo('template_url') ?>/images/work/1.jpg" class="fancybox"> <img src="<?php bloginfo('template_url') ?>/images/work/1.jpg" alt="">
                  <div class="overlay"> <span>Etiam porta</span> </div>
                  </a> </li>
                  <li class="item photography" style="position: absolute; left: 292px; top: 0px;"> <a href="<?php bloginfo('template_url') ?>/images/work/2.jpg" class="fancybox"> <img src="<?php bloginfo('template_url') ?>/images/work/2.jpg" alt="">
                  <div class="overlay"> <span>Lorem ipsum</span> </div>
                  </a> </li>
                  <li class="item branding" style="position: absolute; left: 585px; top: 0px;"> <a href="<?php bloginfo('template_url') ?>/images/work/3.jpg" class="fancybox"> <img src="<?php bloginfo('template_url') ?>/images/work/3.jpg" alt="">
                  <div class="overlay"> <span>Vivamus quis</span> </div>
                  </a> </li>
                  <li class="item photography" style="position: absolute; left: 877px; top: 0px;"> <a href="<?php bloginfo('template_url') ?>/images/work/4.jpg" class="fancybox"> <img src="<?php bloginfo('template_url') ?>/images/work/4.jpg" alt="">
                  <div class="overlay"> <span>Donec ac tellus</span> </div>
                  </a> </li>
                  <li class="item photography" style="position: absolute; left: 0px; top: 219px;"> <a href="<?php bloginfo('template_url') ?>/images/work/5.jpg" class="fancybox"> <img src="<?php bloginfo('template_url') ?>/images/work/5.jpg" alt="">
                  <div class="overlay"> <span>Etiam volutpat</span> </div>
                  </a> </li>
                  <li class="item web" style="position: absolute; left: 292px; top: 219px;"> <a href="<?php bloginfo('template_url') ?>/images/work/6.jpg" class="fancybox"> <img src="<?php bloginfo('template_url') ?>/images/work/6.jpg" alt="">
                  <div class="overlay"> <span>Donec congue </span> </div>
                  </a> </li>
                  <li class="item photography" style="position: absolute; left: 585px; top: 219px;"> <a href="<?php bloginfo('template_url') ?>/images/work/7.jpg" class="fancybox"> <img src="<?php bloginfo('template_url') ?>/images/work/7.jpg" alt="">
                  <div class="overlay"> <span>Nullam a ullamcorper diam</span> </div>
                  </a> </li>
                  <li class="item web" style="position: absolute; left: 877px; top: 219px;"> <a href="<?php bloginfo('template_url') ?>/images/work/8.jpg" class="fancybox"> <img src="<?php bloginfo('template_url') ?>/images/work/8.jpg" alt="">
                  <div class="overlay"> <span>Etiam consequat</span> </div>
                  </a> </li>
                  </ul>
                  </div> */
                ?>
            </div>
        </div>
    </div>
</section>
<section id="team" class="page-section">
    <div class="container">
        <div class="heading text-center"> 
            <!-- Heading -->
            <h2><?php echo do_shortcode("[siteoptions key='team' title='true']") ?></h2>
            <p><?php echo do_shortcode("[subtitle key='team']") ?></p>
            <?php /* <h2>Team</h2>
              <p>At variations of passages of Lorem Ipsum available, but the majority have suffered alteration..</p>
             * 
             */ ?>
        </div>
        <!-- Team Member's Details -->
        <div class="team-content">
            <div class="row">
                <?php echo do_shortcode(do_shortcode("[siteoptions key='team' article=1]")) ?>
                <?php /* <div class="col-md-3 col-sm-6 col-xs-6"> 
                  <!-- Team Member -->
                  <div class="team-member pDark">
                  <!-- Image Hover Block -->
                  <div class="member-img">
                  <!-- Image  -->
                  <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/photo-1.jpg" alt=""> </div>
                  <!-- Member Details -->
                  <h4>John Doe</h4>
                  <!-- Designation -->
                  <span class="pos">CEO</span>
                  <div class="team-socials"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-dribbble"></i></a> <a href="#"><i class="fa fa-github"></i></a> </div>
                  </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-6">
                  <!-- Team Member -->
                  <div class="team-member pDark">
                  <!-- Image Hover Block -->
                  <div class="member-img">
                  <!-- Image  -->
                  <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/photo-2.jpg" alt=""> </div>
                  <!-- Member Details -->
                  <h4>Larry Doe</h4>
                  <!-- Designation -->
                  <span class="pos">Art Director</span>
                  <div class="team-socials"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-dribbble"></i></a> <a href="#"><i class="fa fa-github"></i></a> </div>
                  </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-6">
                  <!-- Team Member -->
                  <div class="team-member pDark">
                  <!-- Image Hover Block -->
                  <div class="member-img">
                  <!-- Image  -->
                  <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/photo-3.jpg" alt=""> </div>
                  <!-- Member Details -->
                  <h4>Ranith Kays</h4>
                  <!-- Designation -->
                  <span class="pos">Manager</span>
                  <div class="team-socials"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-dribbble"></i></a> <a href="#"><i class="fa fa-github"></i></a> </div>
                  </div>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-6">
                  <!-- Team Member -->
                  <div class="team-member pDark">
                  <!-- Image Hover Block -->
                  <div class="member-img">
                  <!-- Image  -->
                  <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/photo-4.jpg" alt=""> </div>
                  <!-- Member Details -->
                  <h4>Joan Ray</h4>
                  <!-- Designation -->
                  <span class="pos">Creative</span>
                  <div class="team-socials"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-dribbble"></i></a> <a href="#"><i class="fa fa-github"></i></a> </div>
                  </div>
                  </div>
                  </div> */ ?>
            </div>
        </div>
        <!--/.container--> 
</section>
<section id="contactUs" class="contact-parlex">
    <div class="parlex-back">
        <div class="container">
            <div class="row">
                <div class="heading text-center"> 
                    <!-- Heading --> 

                </div>
            </div>
            <div class="row mrgn30">

                <div class="col-sm-12">
                    <?php
                    $form7 = do_shortcode("[siteoptions key='contact_us']");
                    echo do_shortcode(stripslashes($form7));
                    ?>
                    <!--                    <form method="post" action="" id="contactfrm" role="form">
                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input type="text" class="form-control" name="name" id="name" placeholder="Enter name" title="Please enter your name (at least 2 characters)">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="email" class="form-control" name="email" id="email" placeholder="Enter email" title="Please enter a valid email address">
                                            </div>
                                            <div class="form-group">
                                                <label for="comments">Comments</label>
                                                <textarea name="comment" class="form-control" id="comments" cols="3" rows="5" placeholder="Enter your message…" title="Please enter your message (at least 10 characters)"></textarea>
                                                <button name="submit" type="submit" class="btn btn-lg btn-primary" id="submit">Submit</button>
                                            </div>
                                            <div class="result"></div>
                                        </form>-->

                </div>
            </div>
        </div>
        <!--/.container--> 
    </div>
</section>
<?php
get_footer()?>