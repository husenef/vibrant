<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
add_theme_support('post-thumbnails');

function add_script() {
    /* add style */
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style('isotope', get_template_directory_uri() . '/css/isotope.css');
    wp_enqueue_style('fancybox', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.css');
    wp_enqueue_style('custom-animate', get_template_directory_uri() . '/css/animate.css');
    /* carouserl */
    wp_enqueue_style('owl-carousel', get_template_directory_uri() . '/js/owl-carousel/owl.carousel.css');
    wp_enqueue_style('styles', get_template_directory_uri() . '/css/styles.css');
    /* font awesome */
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/font/css/font-awesome.min.css');

    /* add js */
    $deps = array();
    $ver = null;
    wp_enqueue_script('modernizr', get_template_directory_uri() . '/js/modernizr-latest.js', $deps, $ver, true);
    wp_deregister_script('jquery');
    wp_register_script('jquery', get_template_directory_uri() . '/js/jquery-1.8.2.min.js', $deps, $ver, true);
    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', $deps, $ver, true);
    wp_enqueue_script('isotope', get_template_directory_uri() . '/js/jquery.isotope.min.js', $deps, $ver, true);
    wp_enqueue_script('fancybox', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.pack.js', $deps, $ver, true);
    wp_enqueue_script('jquery-nav', get_template_directory_uri() . '/js/jquery.nav.js', $deps, $ver, true);
    wp_enqueue_script('jquery-fittext', get_template_directory_uri() . '/js/jquery.fittext.js', $deps, $ver, true);
    wp_enqueue_script('waypoints', get_template_directory_uri() . '/js/waypoints.js', $deps, $ver, true);
    wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/js/owl-carousel/owl.carousel.js', $deps, $ver, true);
    wp_enqueue_script('custom', get_template_directory_uri() . '/js/custom.js', $deps, $ver, true);
}

add_action('wp_enqueue_scripts', 'add_script', 100);

function user_extra_fields($profile_fields) {

    // Add new fields
    $profile_fields['position'] = 'Position';
    $profile_fields['twitter'] = 'Twitter URL';
    $profile_fields['facebook'] = 'Facebook URL';
    $profile_fields['googleplus'] = 'Google+ URL';
//    $profile_fields['linkedin'] = 'Linkedin URL';
//    $profile_fields['youtube'] = 'Youtube URL';
    $profile_fields['dribbble'] = 'Dribbble Url';
    $profile_fields['github'] = 'github Url';
    $profile_fields['image'] = 'Image url';
    return $profile_fields;
}

add_filter('user_contactmethods', 'user_extra_fields');

/* register nav menu */
register_nav_menu('top-menu', 'This Primaty Menu');
