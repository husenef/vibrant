<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function vibrant_options() {
    add_theme_page('Theme Option', 'Theme Options', 'manage_options', 'theme_option.php', array('Site_Options', 'init'));
}

add_action('admin_menu', 'vibrant_options');

if (!class_exists('Site_Options')):

    class Site_Options {

        var $siteOptions = null;
        var $optionName = 'siteOptions';
        var $options = array();
        private $domain = 'vibrant';
        var $articles = null;

        function init() {
            $siteOptions = new self();
        }

        function __construct() {
            // Register script in admin
//            add_action('admin_enqueue_scripts', array($this, 'custom_script'));
            $this->custom_script();
            $this->setArticles();
            $this->saveOptions();
            $this->setOptions();
            $this->dashboard();
        }

        function custom_script() {
            wp_enqueue_media();
            wp_enqueue_script('husen', get_template_directory_uri() . '/js/admin/main.js');
        }

        function setOptions() {
            $options = get_option($this->optionName);
            $this->options = json_decode($options);
        }

        function saveOptions() {
            if ($_POST):
                $value = json_encode($_POST);
                if (!get_option($this->optionName)):
                    add_option($this->optionName, $value);
                else:
                    update_option($this->optionName, $value);
                endif;
            endif;
        }

        function dashboard() {
            ?>
            <div class="wrap">
                <h2>Site Options</h2>
                <form action="" method="post">
                    <h3>
                        <?php echo __('Slider', $this->domain) ?>
                    </h3>
                    <table class="form-table">
                        <tbody>
                            <tr>
                                <th>
                                    <label for="imgSlider"><?php echo __('Image Slider', $this->domain) ?></label>
                                </th>
                                <td>
                                    <input type="text" placeholder="Add URL image here" name="imgSlider" id="imgSlider" class="url" value="<?php echo ($this->returnValue('imgSlider')) ?>" />
                                    <button class="button upload-button" type="button">Upload</button>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <label for="textSlider">
                                        <?php echo __('Text Slider', $this->domain) ?>
                                    </label>
                                </th>
                                <td>
                                    <textarea cols="30" rows="5" id="textSlider" name="textSlider"><?php echo ($this->returnValue('textSlider')) ?></textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <h3>
                        Features
                    </h3>
                    <table class="form-table">
                        <tbody>
                            <tr>
                                <th>
                                    <label for="features"><?php echo __('Feature Article', $this->domain) ?></label>
                                </th>
                                <td>
                                    <?php $this->generateSelect(array('name' => 'features', 'id' => 'features', 'value' => $this->returnValue('features'))) ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <h3>
                        About Us
                    </h3>
                    <table class="form-table">
                        <tbody>
                            <tr>
                                <th>
                                    <label for="about"><?php echo __('About Us Article', $this->domain) ?></label>
                                </th>
                                <td>
                                    <?php $this->generateSelect(array('name' => 'about', 'id' => 'about', 'value' => $this->returnValue('about'))) ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <h3>
                        Services
                    </h3>
                    <table class="form-table">
                        <tbody>
                            <tr>
                                <th>
                                    <label for="service"><?php echo __('Services Article', $this->domain) ?></label>
                                </th>
                                <td>
                                    <?php $this->generateSelect(array('name' => 'service', 'id' => 'service', 'value' => $this->returnValue('service'))) ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <h3>
                        Works (portofolio)
                    </h3>
                    <table class="form-table">
                        <tbody>
                            <tr>
                                <th>
                                    <label for="works"><?php echo __('Work Article', $this->domain) ?></label>
                                </th>
                                <td>
                                    <?php $this->generateSelect(array('name' => 'works', 'id' => 'works', 'value' => $this->returnValue('works'))) ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <h3>
                        Team
                    </h3>
                    <table class="form-table">
                        <tbody>
                            <tr>
                                <th>
                                    <label for="team"><?php echo __('Team Article', $this->domain) ?></label>
                                </th>
                                <td>
                                    <?php $this->generateSelect(array('name' => 'team', 'id' => 'team', 'value' => $this->returnValue('team'))) ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <h3>
                        Contact Us
                    </h3>
                    <table class="form-table">
                        <tbody>
                            <tr>
                                <th>
                                    <label for="contact_us"><?php echo __('Form Id', $this->domain) ?></label>
                                </th>
                                <td>
                                    <textarea name="contact_us" placeholder="7form shortcode"><?php echo stripslashes($this->returnValue('contact_us')) ?></textarea>
                                    <p class="description">Get 7 form id in <a href='<?php echo admin_url('admin.php?page=wpcf7') ?>' target="blank">here</a>, <br/>if you don't have this plugin, can download <a href='https://wordpress.org/plugins/contact-form-7/' target="blank">here</a>.</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <h3>
                        Social Media
                    </h3>
                    <table class="form-table">
                        <tbody>
                            <tr>
                                <th>
                                    <label for="twitter"><?php echo __('Twitter Url', $this->domain) ?></label>
                                </th>
                                <td>
                                    <input type="text" id="twitter" name="twitter" value="<?php echo stripslashes($this->returnValue('twitter')) ?>" placeholder="Twitter URL" />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <label for="fb"><?php echo __('Facebook Url', $this->domain) ?></label>
                                </th>
                                <td>
                                    <input id="fb" type="text" name="facebook" value="<?php echo stripslashes($this->returnValue('facebook')) ?>" placeholder="Facebook URL" />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <label for="dribbble"><?php echo __('Dribbble Url', $this->domain) ?></label>
                                </th>
                                <td>
                                    <input type="text" id="dribbble" name="dribbble" value="<?php echo stripslashes($this->returnValue('dribbble')) ?>" placeholder="Dribbble URL" />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <label for="flickr"><?php echo __('Flickr Url', $this->domain) ?></label>
                                </th>
                                <td>
                                    <input type="text" id="flickr" name="flickr" value="<?php echo stripslashes($this->returnValue('flickr')) ?>" placeholder="Flickr URL" />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <label for="github"><?php echo __('Github Url', $this->domain) ?></label>
                                </th>
                                <td>
                                    <input type="text" name="github" id="github" value="<?php echo stripslashes($this->returnValue('github')) ?>" placeholder="Github URL" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <p class="submit">
                        <input type="submit" value="Save" class="button button-primary" id="submit" />
                    </p>
                </form>
            </div>
            <?php
        }

        function setArticles() {
            $args = array(
                'posts_per_page' => -1,
                'offset' => 0,
                'category' => '',
                'category_name' => '',
                'orderby' => 'date',
                'order' => 'DESC',
                'include' => '',
                'exclude' => '',
                'meta_key' => '',
                'meta_value' => '',
                'post_type' => 'post',
                'post_mime_type' => '',
                'post_parent' => '',
                'post_status' => 'publish',
                'suppress_filters' => true
            );
            $posts_array = get_posts($args);
            wp_reset_postdata();
            $args = array(
                'posts_per_page' => -1,
                'offset' => 0,
                'category' => '',
                'category_name' => '',
                'orderby' => 'date',
                'order' => 'DESC',
                'include' => '',
                'exclude' => '',
                'meta_key' => '',
                'meta_value' => '',
                'post_type' => 'page',
                'post_mime_type' => '',
                'post_parent' => '',
                'post_status' => 'publish',
                'suppress_filters' => true
            );
            $page_array = get_posts($args);
            wp_reset_postdata();

            $this->articles = array(
                'Post' => $posts_array,
                'Page' => $page_array
            );
//            $this->articles->post = $posts_array;
//            $this->articles->page = $page_array;
            return (object) $this->articles;
        }

        function generateSelect($attr = array()) {
            $defaults = array(
                'name' => 'nama',
                'id' => 'id',
                'class' => 'class',
                'value' => null
            );

            $r = wp_parse_args($attr, $defaults);
            ?>
            <select name="<?php echo $r['name'] ?>" id="<?php echo $r['id'] ?>">
                <?php
                foreach ($this->articles as $key => $value) {
                    echo "<optgroup label='$key'>";
                    foreach ($value as $kp => $post) {
                        setup_postdata($post);
                        $selected = ($r['value'] == $post->ID) ? 'selected="selected"' : '';
                        echo "<option value='$post->ID' $selected>$key - $post->post_title</option>";
                    }
                    echo "</optgroup>";
                }
                ?>
            </select>
            <?php
        }

        function returnValue($key = null) {
            if ($key == null)
                return "";
            return $this->options->$key;
        }

    }

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    
endif;