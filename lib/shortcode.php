<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * @name : add shortcode siteoptions
 * @descriptions :  -@key : key on site options
 *                  -@article : default false, true show article
 *                  -@title : default False, true show title
 */
add_shortcode("siteoptions", "get_site_options");

function get_site_options($attr, $cont) {

    if (empty($attr))
        return '';
    $default = array(
        $key => null,
        $article => FALSE,
        $title => FALSE
    );
    $r = wp_parse_args($attr, $default);
    /*
     * $key list
     * Class Object ( 
     * [imgSlider] => url img 
     * [textSlider] => text 
     * [features] => 1 
     * [about] => 3 
     * [service] => 2 
     * [works] => 1 
     * [team] => 2 
     * [contact_us] => 231 )
     */
    $options = get_option('siteOptions');
    $options = json_decode($options);
    if ($r['article']):
        return get_article($options->$r['key'], 'article');
    elseif ($r['title']):
        return get_article($options->$r['key'], 'title');
    else:
        return $options->$r['key'];
    endif;
}

/*
 * @name : add shortcode subtitle
 * @descriptions :  @key = key on site option, to generate id post,
 *                  after get id, generate post meta with id post
 */

function get_subtitle($attr = array(), $content = array()) {

    $post = get_site_options($attr, $content);

    if (!$post)
        return '';
    $postMeta = get_post_meta($post, 'subtitle');
    return $postMeta[0];
}

add_shortcode('subtitle', 'get_subtitle');

function get_article($id = null, $option = 'article') {
    $post = get_post($id);
    if ($post):
        setup_postdata($post);

        if ($option == 'title'):
            $return = $post->post_title;
        else :
            $return = $post->post_content;
        endif;
    else :
        $return = '';
    endif;

    wp_reset_postdata();
    return $return;
}

add_shortcode("featured", 'featuredimage');

function featuredimage($attr = array(), $content = array()) {
    $post = get_site_options($attr, $content);
    if (!$post)
        return '';
    return get_the_post_thumbnail($post, 'full', array('class' => 'img-responsive'));
}

remove_shortcode('gallery');
add_shortcode('gallery', 'custom_gal');

function custom_gal($attr, $contet) {
    $include = $attr['ids'];
    $include = preg_replace('/[^0-9,]+/', '', $include);
    $attachmentArgs = array(
        'include' => $include,
        'post_status' => 'inherit',
        'post_type' => 'attachment',
        'post_mime_type' => 'image',
        'order' => 'ASC',
        'orderby' => 'menu_order ID'
    );
    $attachments = get_posts($attachmentArgs);
//    echo "<pre>";
//    print_r($attachments);
//    echo "</pre>";
    if ($attachments):
        $html = "<div id='portfolio'>";

        $menugalls = array();
        $galls .= "<ul class='items list-unstyled clearfix animated fadeInRight showing' data-animation='fadeInRight' style='position: relative; height: 438px;'>";

        foreach ($attachments as $key => $imgs) {
            $menugalls[$imgs->post_excerpt] = $imgs->post_excerpt;
            $galls .= "<li class='item $imgs->post_excerpt img-$imgs->ID' > ";
            $galls .= "<a href='$imgs->guid' class='fancybox'>";
            $galls .= "<img src='$imgs->guid'  alt=''/>";
            $galls.= "<div class='overlay'><span>$imgs->post_title</span></div>";
            $galls .="</a>";
            $galls .= "</li>";
        }
        $galls .="</ul>";
        $bfore = "<ul class='filters list-inline text-center'>";
        $bfore .= "<li> <a class='active' data-filter='*' href='#'>All</a> </li>";
        foreach ($menugalls as $k => $value) {
            $bfore .="<li><a href='#' data-filter='.$value'>$value</a></li>";
        }
//        <li> <a class = "active" data-filter = "*" href = "#">All</a> </li>
//        <li> <a data-filter = ".photography" href = "#">Photography</a> </li>
//        <li> <a data-filter = ".branding" href = "#">Branding</a> </li>
//        <li> <a data-filter = ".web" href = "#">Web</a> </li>
        $bfore .="</ul>";
        $html .=$bfore;
        $html .=$galls;
        $html .= "</div>";
        return $html;
    else:
        return '';
    endif;
}

//user shortcode
function team_shortcode($atts, $content = null) {

    $atts = shortcode_atts(
            array(
        'id' => '',
        'style' => '',
            ), $atts);


    $ids = explode(',', $atts['id']);

    foreach ($ids as $id) {

//        $email = get_the_author_meta(user_email, $id);
//        if ($atts['style'] == 3 OR $atts['style'] == 4) {
//            $image = get_avatar($email, 100);
//        } else {
//            $image = get_avatar($email, 512);
//        }


        $display_name = get_the_author_meta(display_name, $id);
        $position = get_the_author_meta(position, $id);
        $facebook = get_the_author_meta(facebook, $id);
        $twitter = get_the_author_meta(twitter, $id);
        $googleplus = get_the_author_meta(googleplus, $id);
        $dribbble = get_the_author_meta(dribbble, $id);
        $github = get_the_author_meta(github, $id);
//        $linkedin = get_the_author_meta(linkedin, $authorid);
//        $youtube = get_the_author_meta(youtube, $authorid);
        $image = get_the_author_meta('image', $id);




//        $count.= "<div class='single-member' id='style-" . $atts['style'] . "'>";
//        $count.= "<div class='single-member-thumbnail'>" . $image . "</div>";
//        $count.= "<div class='single-member-name'>" . $display_name . "</div>";
//        $count.= "<div class='single-member-position'>" . $position . "</div>";
//        $count.= "<div class='social-links'>";
//        $count.= "<div class='facebook sl'><a href='" . $facebook . "'></a></div>";
//        $count.= "<div class='twitter sl'><a href='" . $twitter . "'></a></div>";
//        $count.= "<div class='googleplus sl'><a href='" . $googleplus . "'></a></div>";
//        $count.= "<div class='linkedin sl'><a href='" . $linkedin . "'></a></div>";
//        $count.= "<div class='youtube sl'><a href='" . $youtube . "'></a></div>";
//        $count.= "<div class='skype sl'><a href='skype:" . $skype . "?call'></a></div>";
//        $count.= "</div>";
//        $count.= "</div>";
        $count .="<div class='col-md-3 col-sm-6 col-xs-6'> 
                  <!-- Team Member -->
                  <div class='team-member pDark'>
                  <!-- Image Hover Block -->
                  <div class='member-img'>
                  <!-- Image  -->
                  <img class='img-responsive' src='$image' alt=''> </div>
                  <!-- Member Details -->
                  <h4>$display_name</h4>
                  <!-- Designation -->
                  <span class='pos'>$position</span>
                  ";
        $count .= "<div class='team-socials'>"
                . " <a href='$facebook'><i class='fa fa-facebook'></i></a>"
                . " <a href='$googleplus'><i class='fa fa-google-plus'></i></a>"
                . " <a href='$twitter'><i class='fa fa-twitter'></i></a>"
                . " <a href='$dribbble'><i class='fa fa-dribbble'></i></a>"
                . " <a href='$github'><i class='fa fa-github'></i></a>"
                . "</div>";
        $count.="</div>
                  </div>";
//        $count .="<div class='col-md-3 col-sm-6 col-xs-6' id='user-$authorid'>";
//        $count .= "<div class='team-member pDark'>";
//        $count .= "<div class='member-img'>";
//        $count .="<img class='img-responsive' src='$image' alt=''> ";
//        $count .="</div><!-- /.member-img -->";
//        $count .="</div><!-- /.pDark -->";
//        $count .="<h4>$display_name</h4>Designation<span class='pos'>$position</span>";
//        $count .="</div>";
        /*
         * <div class="col-md-3 col-sm-6 col-xs-6"> 
          Team Member
          <div class="team-member pDark">
          Image Hover Block
          <div class="member-img">
          Image
          <img class="img-responsive" src="<?php bloginfo('template_url') ?>/images/photo-1.jpg" alt="">
          </div>
          Member Details
          <h4>John Doe</h4>
          Designation
          <span class="pos">CEO</span>
          <div class="team-socials"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-dribbble"></i></a> <a href="#"><i class="fa fa-github"></i></a> </div>
          </div>
          </div>
         */
    }


    return $count;
//    return "<div class='kento-team'>" . $count . "</div>";
}

add_shortcode('team', 'team_shortcode');
