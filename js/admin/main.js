jQuery(function() {

	var custom_uploader;

	jQuery('.upload-button').on('click', function(e) { 
		e.preventDefault();

		var button = jQuery(this);

		// if (custom_uploader) {
  //           custom_uploader.open();
  //           return;
  //       }
 
        //Extend the wp.media object
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            },
            multiple: false
        });
 
        //When a file is selected, grab the URL and set it as the text field's value
        custom_uploader.on('select', function() {
            attachment = custom_uploader.state().get('selection').first().toJSON();
            button.siblings('.url').val(attachment.url);
        });
 
        //Open the uploader dialog
        custom_uploader.open();
	});

	// jQuery('#save-all-slider').on('click', function(e) {
	// 	e.preventDefault();
	// 	console.log('clicked');
	// });

    jQuery('.delete-image').on('click', function(event) {
        event.preventDefault();
        jQuery(this).parent().parent().find('.url').val('');
        jQuery(this).parent().parent().find('.title').val('');
        jQuery(this).parent().parent().find('.main-image').remove();
        jQuery(this).parent().parent().find('.replace-image').removeClass('hidden');
        console.log(jQuery(this).parent().parent().find('.replace-image'));
        //.removeClass('hidden').addClass('appear');
    });
});