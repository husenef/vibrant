<footer>
    <div class="container">
        <div class="social text-center"> 
            <a href="<?php echo do_shortcode("[siteoptions key='twitter']")?>"><i class="fa fa-twitter"></i></a>
            <a href="<?php echo do_shortcode("[siteoptions key='facebook']")?>"><i class="fa fa-facebook"></i></a>
            <a href="<?php echo do_shortcode("[siteoptions key='dribbble']")?>"><i class="fa fa-dribbble"></i></a>
            <a href="<?php echo do_shortcode("[siteoptions key='flickr']")?>"><i class="fa fa-flickr"></i></a>
            <a href="<?php echo do_shortcode("[siteoptions key='github']")?>"><i class="fa fa-github"></i></a> 
        </div>
        <div class="clear"></div>
        <!--CLEAR FLOATS--> 
    </div>
</footer>
<!--/.page-section-->
<section class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center"> Copyright 2014 | All Rights Reserved -- Template by <a href="http://webThemez.com">WebThemez.com</a> </div>
        </div>
        <!-- / .row --> 
    </div>
</section>
<a href="#top" class="topHome"><i class="fa fa-chevron-up fa-2x"></i></a> 

        <!--[if lte IE 8]><script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script><![endif]--> 

<?php wp_footer() ?>
</body>
</html>
